package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}

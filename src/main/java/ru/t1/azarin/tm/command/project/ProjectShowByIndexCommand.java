package ru.t1.azarin.tm.command.project;

import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    public final static String NAME = "project-show-by-index";

    public final static String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[FIND PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final String userId = getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

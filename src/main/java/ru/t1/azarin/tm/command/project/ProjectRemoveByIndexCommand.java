package ru.t1.azarin.tm.command.project;

import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public final static String NAME = "project-remove-by-index";

    public final static String DESCRIPTION = "Remove project by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final String userId = getUserId();
        final Project project = serviceLocator.getProjectService().removeByIndex(userId, index);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

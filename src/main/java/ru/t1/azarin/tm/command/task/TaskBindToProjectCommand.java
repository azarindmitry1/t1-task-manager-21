package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public final static String NAME = "task-bind-to-project";

    public final static String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

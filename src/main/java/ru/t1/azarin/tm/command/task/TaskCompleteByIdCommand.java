package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    public final static String NAME = "task-complete-by-id";

    public final static String DESCRIPTION = "Complete task by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
